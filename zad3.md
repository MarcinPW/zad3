---
author: Marcin Kuklewski
title: Latarki
subtitle: Latarki do zastosowań na codzień
date: 05.11.2023
theme: Warsaw
output: beamer_presentation
header-includes:
	\usepackage{xcolor}
	\usepackage{caption}
---

## Slajd 1

**Latarki dla każdego na każdą okazję**

---

## Slajd 2

Latarki możemy podzielić według zastosowania:

- EDC - tak zwane "na codzień" (EDC - Every Day Carry)
- turystyczne i survivalowe - do zastosowań podczas pieszych wędrówek lub prób przeżycia na odludziu
- czołowe - bardzo pomocne przy pokonywaniu przeszkód gdzie potrzebujemy mieć obie ręce wolne
- rowerowe - latarki te charakteryzują się "ostrym" odcięciem pola świecenia aby nie oślepiać innych osób

---

## Slajd 3

**EDC - Every Day Carry**

Latarki tego typu, oprócz wygodnego mocowania, mogą być spełniać inne role. Po za funkcją bycia bardzo drogim przyciskiem do papieru mogą służyć jako PowerBank

<img src="Rys1.jpg" alt="EDC" width="400">

---

## Slajd 4

**Turystyczne i survivalowe**

To jest kategoria dla najbardziej wymagających. Światełka te muszą wytrzymać nie tylko kiepskie warunki pogodowe ale także i nie jedno uderzenie czy upadek.

Są zazwyczaj stworzone do konkretnego celu bądż jednej konkretnej funkcji np. mają świecić daleko ale wąskim światłem, blisko ale oświetlać wszystko w okół nas bądź po prostu oświetlać w miare szeroko i w miare małej odległości

<img style="right;" src="Rys2.jpg" alt="Turystyczna" width="300">
<img style="left;" src="Rys3.jpg" alt="Szperacz" width="300">

---

## Slajd 5

**Czołowe**

To jest rodzaj latarek dla osób uprawiających sport, wspinających się czy pracujących w miejscach zaciemnionych i potrzebujących mieć obie recę wolne.

<img src="Rys4.jpg" alt="czołówka" width="300">

---

## Slajd 6

**Rowerowe**

Ich mocowanie pozwala na bezpośrednie przypiecie lampki tak, że oświetla drogę dokładnie przed nami

<img src="Rys5.jpg" alt="czołówka" width="300">

---

## Slajd 7

**Diody zastosowane w latarkach**

Obecnie znane mi są diody oraz moduły diodowe w kolejności w zależności od **mocy**:

1. CREE XHP 70
2. CREE XHP 50
3. CREE XHP 35
4. CREE XP-G
5. CREE XM-L

---

## Slajd 8

**Temperatury barwowe**

Najczęstszymi i najbardziej popularnymi temperaturami barwowymi podawanymi w Kelvinach są:

- 2700K
- 2800K
- 4000K
- 5500K
- 6500K

![image](Rys6.jpg)

---

## Slajd 9

**Zastosowanie poszczególnych temperatur**

| Salon  |  Kuchnia  | Łazienka  | Pracownia |
| :----: | :-------: | :-------: | :-------: |
| Ciepła | Neutralna | Neutralna |   Zimna   |
